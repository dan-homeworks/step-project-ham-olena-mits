# Ham project

Step Project "Ham"

## Description
Step project after basic JS module.

Simple web-page built on pure css and javascript. The additional library is Slicker JS to create cards and feedback carousel.

## Used technologies

- HTML 5
- CSS 3
- Vanilla JS
- Slicker JS


## Tasks

- create HTML page
- create css loader
- create cart and feedback carousel with SlickerJS