"use strict"

const testimonials = [
    {
        "id": 0,
        "name": "Monica Geller",
        "jobTitle": "Web Developer",
        "imageUrl": "./images/feedback/one.svg",
        "comment": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi eaque fugit quis quidem distinctio. Ex eaque, voluptatum, quae in iusto animi quos ducimus nulla placeat architecto distinctio!"
    },
    {
        "id": 1,
        "name": "Ross Geller",
        "jobTitle": "Software Developer",
        "imageUrl": "./images/feedback/two.svg",
        "comment": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    {
        "id": 2,
        "name": "Joseph Tribbiani",
        "jobTitle": "Project Manager",
        "imageUrl": "./images/feedback/three.svg",
        "comment": "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias rem id dolorem maiores quisquam odit, enim repellendus ea. Hic possimus alias eum odio amet velit facere mollitia esse deleniti sit!"
    },
    {
        "id": 3,
        "name": "Fibi Buffai",
        "jobTitle": "Web Designer",
        "imageUrl": "./images/feedback/four.svg",
        "comment": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    {
        "id": 4,
        "name": "Rachel Green",
        "jobTitle": "Product Manager",
        "imageUrl": "./images/feedback/five.jpg",
        "comment": "Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
    {
        "id": 5,
        "name": "Chandler Bing",
        "jobTitle": "UX Designer",
        "imageUrl": "./images/feedback/six.jpg",
        "comment": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }
];

const personInfo = (index) => {
    const objectInfo = testimonials[index]
    
    $('.feedback__text').text(objectInfo.comment);
    $('.feedback__name').text(objectInfo.name);
    $('.feedback__title-name').text(objectInfo.jobTitle);
};

$(document).ready(() => {
    $.each(testimonials, (i, {imageUrl}) => {
        const slideContent = `
        <div>
            <div class="slick__slide">
                <div class="slick__slide-img" style="background-image: url(${imageUrl})"></div>
            </div>
        </div>
        `;

        $('.slider-for')
            .append(slideContent);
        $('.slider-nav')
            .append(slideContent);
        personInfo(0);
    })
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
      });
    $('.slider-nav').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true,
        prevArrow: $('.previous__btn'),
        nextArrow: $('.next__btn')
    });

    $('.slider-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        personInfo(nextSlide);
    });
});