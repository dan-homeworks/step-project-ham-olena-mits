"use strict"

$('#service-tabs').on('click', (event) => {
    const dataName = event.target.dataset.tab;
    $('.active__title').removeClass('active__title');
    event.target.classList.add('active__title');
    
    $('.visible__content').removeClass('visible__content');
    $(`[data-content = '${dataName}']`).addClass('visible__content');
});