"use strict"

const works = [
    {
        "category": "graphicdesign",
        "id": "graphic-design1",
        "url": "./images/graphic_design/graphic-design1.jpg",
        "category_name": "Graphic Design",
        "service_name": "Logo Design"

    },
    {
        "category": "graphicdesign",
        "id": "graphic-design2",
        "url": "./images/graphic_design/graphic-design2.jpg",
        "category_name": "Graphic Design",
        "service_name": "Booklet Design"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design3",
        "url": "./images/graphic_design/graphic-design3.jpg",
        "category_name": "Graphic Design",
        "service_name": "Catalog Design"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design4",
        "url": "./images/graphic_design/graphic-design4.jpg",
        "category_name": "Graphic Design",
        "service_name": "Calender Design"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design5",
        "url": "./images/graphic_design/graphic-design5.jpg",
        "category_name": "Graphic Design",
        "service_name": "Banner Design"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design6",
        "url": "./images/graphic_design/graphic-design6.jpg",
        "category_name": "Graphic Design",
        "service_name": "T-shirt design"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design7",
        "url": "./images/graphic_design/graphic-design7.jpg",
        "category_name": "Graphic Design",
        "service_name": "Newsletter design"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design8",
        "url": "./images/graphic_design/graphic-design8.jpg",
        "category_name": "Graphic Design",
        "service_name": "Exhibition Designs"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design9",
        "url": "./images/graphic_design/graphic-design9.jpg",
        "category_name": "Graphic Design",
        "service_name": "Invitation Card"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design10",
        "url": "./images/graphic_design/graphic-design10.jpg",
        "category_name": "Graphic Design",
        "service_name": "Business Leaflets"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design11",
        "url": "./images/graphic_design/graphic-design11.jpg",
        "category_name": "Graphic Design",
        "service_name": "Corporate Brochure"
    },
    {
        "category": "graphicdesign",
        "id": "graphic-design12",
        "url": "./images/graphic_design/graphic-design12.jpg",
        "category_name": "Graphic Design",
        "service_name": "Stationary Design"
    },
    {
        "category": "landingpage",
        "id": "landing_page1",
        "url": "./images/landing_page/landing-page1.jpg",
        "category_name": "Landing Pages",
        "service_name": "Shopify"
    },
    {
        "category": "landingpage",
        "id": "landing_page2",
        "url": "./images/landing_page/landing-page2.jpg",
        "category_name": "Landing Pages",
        "service_name": "Muzzle"
    },
    {
        "category": "landingpage",
        "id": "landing_page3",
        "url": "./images/landing_page/landing-page3.jpg",
        "category_name": "Landing Pages",
        "service_name": "Webflow"
    },
    {
        "category": "landingpage",
        "id": "landing_page4",
        "url": "./images/landing_page/landing-page4.jpg",
        "category_name": "Landing Pages",
        "service_name": "Wistia"
    },
    {
        "category": "landingpage",
        "id": "landing_page5",
        "url": "./images/landing_page/landing-page5.jpg",
        "category_name": "Landing Pages",
        "service_name": "Teambit"
    },
    {
        "category": "landingpage",
        "id": "landing_page6",
        "url": "./images/landing_page/landing-page6.jpg",
        "category_name": "Landing Pages",
        "service_name": "Airbnb"
    },
    {
        "category": "landingpage",
        "id": "landing_page7",
        "url": "./images/landing_page/landing-page7.jpg",
        "category_name": "Landing Pages",
        "service_name": "TransferWise"
    },
    {
        "category": "webdesign",
        "id": "web_design1",
        "url": "./images/web_design/web-design1.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "webdesign",
        "id": "web_design2",
        "url": "./images/web_design/web-design2.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "webdesign",
        "id": "web_design3",
        "url": "./images/web_design/web-design3.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "webdesign",
        "id": "web_design4",
        "url": "./images/web_design/web-design4.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "webdesign",
        "id": "web_design5",
        "url": "./images/web_design/web-design5.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "webdesign",
        "id": "web_design6",
        "url": "./images/web_design/web-design6.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "webdesign",
        "id": "web_design7",
        "url": "./images/web_design/web-design7.jpg",
        "category_name": "Web Design",
        "service_name": "Creative Design"
    },
    {
        "category": "wordpress",
        "id": "wordpress1",
        "url": "./images/wordpress/wordpress1.jpg",
        "category_name": "Wordpress",
        "service_name": "Industrial Strength"
    },
    {
        "category": "wordpress",
        "id": "wordpress2",
        "url": "./images/wordpress/wordpress2.jpg",
        "category_name": "Wordpress",
        "service_name": "Inbound Emotion"
    },
    {
        "category": "wordpress",
        "id": "wordpress3",
        "url": "./images/wordpress/wordpress3.jpg",
        "category_name": "Wordpress",
        "service_name": "Velaro Live Chat"
    },
    {
        "category": "wordpress",
        "id": "wordpress4",
        "url": "./images/wordpress/wordpress4.jpg",
        "category_name": "Wordpress",
        "service_name": "Unbounce"
    },
    {
        "category": "wordpress",
        "id": "wordpress5",
        "url": "./images/wordpress/wordpress5.jpg",
        "category_name": "Wordpress",
        "service_name": "Trulia"
    },
    {
        "category": "wordpress",
        "id": "wordpress6",
        "url": "./images/wordpress/wordpress6.jpg",
        "category_name": "Wordpress",
        "service_name": "Landbot"
    },
    {
        "category": "wordpress",
        "id": "wordpress7",
        "url": "./images/wordpress/wordpress7.jpg",
        "category_name": "Wordpress",
        "service_name": "Webprofits"
    },
    {
        "category": "wordpress",
        "id": "wordpress8",
        "url": "./images/wordpress/wordpress8.jpg",
        "category_name": "Wordpress",
        "service_name": "Shopify"
    },
    {
        "category": "wordpress",
        "id": "wordpress9",
        "url": "./images/wordpress/wordpress9.jpg",
        "category_name": "Wordpress",
        "service_name": "Conversion Lab"
    },
    {
        "category": "wordpress",
        "id": "wordpress10",
        "url": "./images/wordpress/wordpress10.jpg",
        "category_name": "Wordpress",
        "service_name": "Nauto"    
    }
]

const paging = {
    page: 0,
    pageSize: 12,
    filter: "all",
    get getFilter() {
        return this.filter;
    },
    set setFilter(value) {
        this.page = 0;
        this.filter = value;
    },
    get getItemsCount() {
        return this.pageSize * (this.page + 1);
    },
    set setPage(value) {
        this.page = value;
    },
    get getOffset() {
        return this.pageSize * this.page; 
    },
};

Object.defineProperty(paging, "pageSize", { writable: false });

function getData(count, filter = "all", offset = 0) {
    const data = filter === "all" ? [...works] : works.filter(({ category }) => category === filter);
    const remainedCount = data.length - count || 0;
    return { data: data.slice(offset, count), remainedCount };
};

function renderItems(data) {
    data.forEach(({ url, category_name, service_name }) => {
        const card = `
            <li class="tabs__work__item">
            <div class="item__inner"> 
                <div class="item__inner-front" style="background-image: url(${url})"></div>
                <div class="item__inner-back">
                    <div class="card__icons">
                        <div class="card__icon card__icon1"></div>
                        <div class="card__icon card__icon2">
                            <img src="./images/card__icon2.svg" alt="Card icon">
                        </div>
                    </div>
                    <p class="service__name">${service_name}</p>
                    <p class="category__name">${category_name}</p>
                </div>
            </div>
            </li>
        `;
        
        $('.tabs__content__work').append(card);
    });
};

$('.work__tabs').click(function(event) {
    const category = event.target.dataset.category;
    paging.setFilter = category;
    const data = getData(paging.getItemsCount, paging.getFilter, paging.getOffset);
    $('.tabs__content__work').empty();
    renderItems(data.data);   
    if(data.remainedCount > 0) {
        $('#load__work-cards').show()
    } else if(data.remainedCount === 0 || data.remainedCount < 0) {
        $('#load__work-cards').hide();
    };
});


$('#load__work-cards').click(() => {
    $('#load__work-cards').hide();
    $('.loader__wrapper').show();

    setTimeout(function () {
        paging.setPage = paging.page + 1;
        const data = getData(paging.getItemsCount, paging.getFilter, paging.getOffset);
        renderItems(data.data);
        
        
        $('.loader__wrapper').hide();
        $('#load__work-cards').show();
        if(data.remainedCount === 0) {
            $('#load__work-cards').hide();
        };
    }, 2000);
});

$(document).ready(function () {
    const data = getData(paging.getItemsCount, paging.getFilter, paging.getOffset);
    renderItems(data.data);
    $('.loader__wrapper').hide();
});